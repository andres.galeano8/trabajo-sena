using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Movimiento del personaje")]
    public float speedMovement, runMultiplier, jumpForce;
    Rigidbody2D rb2D;
    int jumpCounter;
    public bool isGround;
    public Vector2 offsetGround;
    public float offsetRadius;
    public LayerMask ground;
    public float raycastDistance, upRaycastRange, downRaycastRange;
    public Vector2 upRaycast, downRaycast;
    SpriteRenderer sr;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb2D = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        Jump();
        GripCorner();
    }
    private void FixedUpdate()
    {
        Movement();
    }
    void Movement()
    {
        rb2D.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * speedMovement, rb2D.velocity.y);
        sr.flipX = Input.GetAxisRaw("Horizontal")<0;
    }
    void Jump()
    {
        if (Input.GetButtonDown("Jump")&jumpCounter>0)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpForce);
            jumpCounter--;
        }
        isGround=Physics2D.OverlapCircle((Vector2)transform.position + offsetGround, offsetRadius, ground);
        if (isGround)
        {
            jumpCounter = 1;
        }
    }
    void GripCorner()
    {
        float dir = sr.flipX ? -1 : 1;
        Debug.DrawRay((Vector2)transform.position + upRaycast, Vector2.right * dir * upRaycastRange, Color.blue);
        RaycastHit2D upperRaycast = Physics2D.Raycast((Vector2)transform.position + upRaycast, Vector2.left * upRaycastRange);
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere((Vector2)transform.position + offsetGround, offsetRadius);
    }
}
