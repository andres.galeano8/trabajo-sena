using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class MapMaker : MonoBehaviour
{
    public Tilemap tilemap;
    public Tile tile;
    public int mapWidth, mapHeight;
    private int[,] mapData;
    public CellularData cellularData;
    public PerlinData perlinData;
    // Start is called before the first frame update
    void Start()
    {
        //PerlinData
        //this.mapData = this.perlinData.GenerateData(this.mapWidth,this.mapHeight);
        //this.GenerateTiles();
        //CellularData
        this.mapData = this.cellularData.GenerateData(this.mapWidth, this.mapHeight);
        this.GenerateTiles();
    }

    void GenerateTiles ()
    {
        for (int i = 0; i < this.mapWidth; i++)
        {
            for (int j = 0; j < this.mapHeight; j++)
            {
                if (this.mapData[i, j]==1)
                {
                    this.tilemap.SetTile(new Vector3Int(i, j, 0), this.tile);
                }
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
